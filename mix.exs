defmodule Tmap.MixProject do
  use Mix.Project

  def project do
    [
      app: :tmap,
      version: "21.9.3",
      elixir: ">= 1.10.0",
      # You have to add this extraline
      escript: [main_module: TMap.CLI],
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      description: description(),
      name: "tmap",
      source_url: "https://gitlab.com/matteoredaelli/tmap",
      package: package(),
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:credo, "~> 1.6.4", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.1.0", only: [:dev], runtime: false},
      {:ex_doc, "~> 0.28.3", only: :dev},
      {:jason, "~> 1.3.0"}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
    ]
  end

  defp description do
    """
    TMap is a library for manipulating map objects, useful for master data management systems
    it is possible to define rules for keys/values replace, adding new keys/values by regex
    matching on otehr keys/values,... See Plugins direcctory for details
    """
  end

  defp package do
    [
      files: ~w(lib .formatter.exs mix.exs README* LICENSE*),
      maintainers: ["Matteo Redaelli"],
      licenses: ["GPL V3+"],
      links: %{"GitLab" => "https://gitlab.com/matteoredaelli/tmap"}
    ]
  end
end
