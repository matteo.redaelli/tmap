defmodule TMapTestValuesCase do
  use ExUnit.Case, async: true

  doctest TMap

  test "ValuesCaseUpper" do
    data = [%{"k1" => "vv1", "K2" => "vv2"}]
    rules = [%{"action" => "ValuesCase", "case" => "upper", "keys" => ["k1"]}]
    assert TMap.apply_rules(data, rules) == [%{"k1" => "VV1", "K2" => "vv2"}]
  end

  test "ValuesCaseLower" do
    data = [%{"K1" => "VV1", "k2" => "VV2"}]
    rules = [%{"action" => "ValuesCase", "case" => "lower", "keys" => ["K1"]}]
    assert TMap.apply_rules(data, rules) == [%{"K1" => "vv1", "k2" => "VV2"}]
  end
end
