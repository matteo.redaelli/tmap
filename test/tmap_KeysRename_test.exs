defmodule TMapTestKeysRename do
  use ExUnit.Case, async: true
  doctest TMap

  # 1533042873,
  test " KeysRename" do
    data = [%{"k1" => "v1", "k2" => "v2"}]

    rules = [
      %{
	"action" => "KeysRename",
	"keys" => %{"k1" => "k11", "k3" => "k33"},
	"mode" => "no_overwrite"
      }
    ]

    assert TMap.apply_rules(data, rules) == [%{"k11" => "v1", "k2" => "v2"}]
  end
end
