defmodule TMapTestValueAppend do
  use ExUnit.Case, async: true
  doctest TMap

  test "ValueAppend" do
    data = [%{"k1" => "v1", "k2" => ["v2"]}]
    rules = [%{"action" => "ValueAppend", "key" => "k2", "value" => "v22"}]

    assert TMap.apply_rules(data, rules) == [%{"k1" => "v1", "k2" => ["v22", "v2"]}]
  end

  test "ValueAppendNolist" do
    data = [%{"k1" => "v1", "k2" => "v2"}]
    rules = [%{"action" => "ValueAppend", "key" => "k2", "value" => "v22"}]

    assert TMap.apply_rules(data, rules) == [%{"k1" => "v1", "k2" => ["v22", "v2"]}]
  end

  test "ValueAppendNoKey" do
    data = [%{"k1" => "v1"}]
    rules = [%{"action" => "ValueAppend", "key" => "k2", "value" => "v22"}]

    assert TMap.apply_rules(data, rules) == [%{"k1" => "v1", "k2" => ["v22"]}]
  end
end
