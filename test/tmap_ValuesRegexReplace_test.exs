defmodule TMapTestValuesRegexReplace do
  use ExUnit.Case, async: true

  doctest TMap

  test "ValuesRegexReplace" do
    data = [%{"k1" => "v1", "k2" => "v12"}]
    rules = [%{"action" => "ValuesRegexReplace", "replace_with" => "11", "value_regex" => "1$"}]
    assert TMap.apply_rules(data, rules) == [%{"k1" => "v11", "k2" => "v12"}]
  end

  test "ValuesRegexReplaceYes" do
    data = [%{"k1" => "Sì", "k2" => "YES"}]

    rules = [
      %{"action" => "ValuesRegexReplace", "replace_with" => "TRUE", "value_regex" => "Sì|YES"}
    ]

    assert TMap.apply_rules(data, rules) == [%{"k1" => "TRUE", "k2" => "TRUE"}]
  end

  test "ValuesRegexReplace special characters" do
    data = [%{"k1" => ~s(v1), "k2" => "v1 2"}]
    rules = [%{"action" => "ValuesRegexReplace", "replace_with" => "", "value_regex" => ~s([ ]+)}]
    assert TMap.apply_rules(data, rules) == [%{"k1" => "v1", "k2" => "v12"}]
  end
end
