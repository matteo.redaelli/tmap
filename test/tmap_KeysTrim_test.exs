defmodule TMapTestKeysTrim do
  use ExUnit.Case, async: true
  doctest TMap

  test "KeysTrim" do
    data = [%{"k1 " => " v1", " k2" => "v2 "}]
    rules = [%{"action" => "KeysTrim", "mode" => "no_overwrite"}]
    assert TMap.apply_rules(data, rules) == [%{"k1" => " v1", "k2" => "v2 "}]
  end
end
