defmodule TMapTestFullm do
  use ExUnit.Case, async: true
  doctest TMap

  test "Full1" do
    data = [%{"val" => "(simb.)", "voc" => "2y"}]
    rules = [%{"action" => "ValueRegexReplace", "keys" => ["voc"], "replace_with" => "", "value_regex" => "[0-9]+"}, %{"action" => "ValueRegexReplace", "keys" => ["val"], "replace_with" => "", "value_regex" => "[\\(\\)]"}, %{"action" => "KeysAdd", "keys" => %{"source" => "Internazionale"}, "mode" => "no_overwrite"}]

    assert TMap.apply_rules(data, rules) == [%{"val" => "simb.", "voc" => "y", "source" => "Internazionale"}]
  end
end
