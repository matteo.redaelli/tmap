defmodule TMapTestKeyRegexRename do
  use ExUnit.Case, async: true
  doctest TMap

  test "POST KeyRegexRename" do
    data = [%{"k1" => "v1", "k2" => "v2"}]

    rules = [
      %{
	"action" => "KeyRegexRename",
	"new_key" => "k11",
	"key_regex" => "1$",
	"mode" => "no_overwrite"
      }
    ]

    assert TMap.apply_rules(data, rules) == [%{"k11" => "v1", "k2" => "v2"}]
  end
end
