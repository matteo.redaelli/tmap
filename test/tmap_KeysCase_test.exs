defmodule TMapTestKeysCase do
  use ExUnit.Case, async: true
  doctest TMap

  test "KeysCaseUpper" do
    data = [%{"k1" => "v1"}]
    rules = [%{"action" => "KeysCase", "case" => "upper", "mode" => "no_overwrite"}]

    assert TMap.apply_rules(data, rules) == [%{"K1" => "v1"}]
  end

  test "KeysCaseLower" do
    data = [%{"K1" => "V1"}]
    rules = [%{"action" => "KeysCase", "case" => "lower", "mode" => "no_overwrite"}]
    assert TMap.apply_rules(data, rules) == [%{"k1" => "V1"}]
  end
end
