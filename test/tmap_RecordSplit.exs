defmodule TMapTestRecordSplit do
  use ExUnit.Case, async: true
  doctest TMap

  test "RecordSplit1" do
    data = [%{"k1" => "v1", "k2" => "word1,word2,word3"}]

    rules = [
      %{
	"action" => "RecordSplit",
	"key" => "k2",
	"pattern" => ","
      }
    ]

    assert TMap.apply_rules(data, rules) ==
      [%{ "k1" => "v1",
	  "k2" => "word1"},
       %{ "k1" => "v1",
	  "k2" => "word2"},
       %{ "k1" => "v1",
	  "k2" => "word3"}
      ]
    end
end
