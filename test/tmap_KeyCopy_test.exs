defmodule TMapTestKeyCopy do
  use ExUnit.Case, async: true

  doctest TMap

  test "KeyCopyNoOverwrite" do
    data = [%{"k1" => "v1", "k2" => "v2"}]
    rules = [%{"action" => "KeyCopy", "from_key" => "k2", "to_key" => "k1", "mode" => "no_overwrite"}]

    assert TMap.apply_rules(data, rules) == [%{"k1" => "v1", "k2" => "v2"}]
  end

  test "KeyCopyNew" do
    data = [%{"k1" => "v1", "k2" => "v2"}]
    rules = [%{"action" => "KeyCopy", "from_key" => "k2", "to_key" => "k3", "mode" => "no_overwrite"}]

    assert TMap.apply_rules(data, rules) == [%{"k1" => "v1", "k2" => "v2", "k3" => "v2"}]
  end

  test "KeyCopy_Overwrite" do
    data = [%{"k1" => "v1", "k2" => "v2"}]
    rules = [%{"action" => "KeyCopy", "from_key" => "k2", "to_key" => "k1", "mode" => "overwrite"}]

    assert TMap.apply_rules(data, rules) == [%{"k1" => "v2", "k2" => "v2"}]
  end
end
