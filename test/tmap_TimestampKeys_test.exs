defmodule TMapTestTimestampKeys do
  use ExUnit.Case, async: true
  doctest TMap

  test "TimestampKeys" do
    data = [%{"k1" => "v1"}]
    rules = [%{"action" => "TimestampKeys"}]
    new_data = Enum.at(TMap.apply_rules(data, rules), 0)
    assert ["_created", "_last_update"] -- Map.keys(new_data) == []
  end

  # ,
  test "POST TimestampKeys with _created" do
    data = [%{"_created" => 1_533_042_873, "_last_update" => 1_533_042_873}]
    rules = [%{"action" => "TimestampKeys"}]

    out = Enum.at(TMap.apply_rules(data, rules), 0)
    assert out["_created"] == 1_533_042_873
    assert out["_last_update"] > 1_533_042_873
  end
end
