# Tmap

**TMap is a commandline / elixir library for transforming map (json) objects**

You can add, rename, remove keys, replace values using regex expressions.

See the source code (sample_rules_tyre.json, lib/plugins/*.ex and test/*exs) files for features.

New features are added as plugins. The available plugins are:

- key_add_if.ex
- ley_copy.ex
- key_regex_rename.ex
- keys_add.ex
- keys_case.ex
- keys_delete.ex
- keys_regex_replace.ex
- keys_rename.ex
- keys_select.ex
- keys_trim.ex
- timestamp_keys.ex
- value_append.ex
- value_regex_named_captures.ex
- value_regex_replace.ex
- record_drop.ex
- record_split.ex
- values_case.ex
- values_regex_replace.ex
- values_trim.ex

## Installation

The package is [available in Hex](https://hex.pm/packages/tmap) and can be installed by adding `tmap` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
	{:tmap, "~> 20.12.25"}
  ]
end
```

## Usage as library

See test files for usage samples.

## Usage as command line

Remember to built the exe file with

  mix escript.build

```
cat myjsonfile.json | tmap --rules-file sample_rules_type.json

tmap --rules-file sample_rules_type.json --input-file myjsonfile.json --output-file newjsonfile.json

cat myjsonfile.json | tmap --rules '[{"action": "KeysSelect", "keys": ["brand"]}]'

cat a.json | tmap --rules '[{"action": "ValueRegexReplace", "replace_with": "", "keys": ["voc"], "value_regex": "[0-9]"}]'
```
