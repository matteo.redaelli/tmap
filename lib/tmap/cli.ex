# do not forget the namespace here
defmodule TMap.CLI do
  require Jason
  require Logger

  @spec stream_lines(Enumerable.t(), String.t()) :: Enumerable.t()
  def stream_lines(in_stream, rules) do
    in_stream
    |> Stream.map(&Jason.decode!(&1))
    |> Stream.map(&List.wrap(&1))
    |> Stream.map(&TMap.apply_rules(&1, rules))
    |> Stream.flat_map(fn x -> x end)
    |> Stream.map(&Jason.encode!(&1))
    ##     in_stream |> Stream.map(&Jason.encode!(TMap.apply_rules([Jason.decode!(&1)], rules)))
  end

  def main(args) do
    {opts, _, _} =
      OptionParser.parse(args,
	strict: [
	  input_file: :string,
	  output_file: :string,
	  rules: :string,
	  rules_file: :string
	]
      )

    input_file = opts[:input_file]
    output_file = opts[:output_file]

    rules =
      if is_nil(opts[:rules]) do
	Logger.warn("Missing option '--rules'")
	{:ok, []}
      else
	case Jason.decode(opts[:rules]) do
	  {:error, reason} ->
	    Logger.error(
	      "Invalid json string in rule parameter: " <> "Reason:" <> inspect(reason)
	    )

	    {:ok, []}

	  {:ok, rulesO} ->
	    Logger.debug("Rule parameter: " <> inspect(rulesO))
	    {:ok, rulesO}
	end
      end

    rules_file = opts[:rules_file]

    rules_from_file =
      if is_nil(rules_file) or not File.exists?(rules_file) do
	Logger.warn("--rules-file not passed or cannot open it")
	{:ok, []}
      else
	case File.read(rules_file) do
	  {:error, reason} ->
	    Logger.error("Error opening file " <> rules_file <> "Reason:" <> reason)
	    {:ok, []}

	  {:ok, rules2} ->
	    case Jason.decode(rules2) do
	      {:ok, rules3} ->
		{:ok, rules3}

	      {:error, reason} ->
		Logger.error("Error parsing file " <> rules_file <> "Reason:" <> reason)
		{:ok, []}
	    end
	end
      end

    ## removing all :error, keeping only :ok
    all_rules = List.flatten(Keyword.get_values([rules, rules_from_file], :ok))
    Logger.info(inspect(all_rules))

    input_stream =
      if not is_nil(input_file) and File.exists?(input_file),
	do: File.stream!(input_file),
	else: IO.stream(:stdio, :line)

    output_stream =
      if not is_nil(output_file), do: File.stream!(output_file), else: IO.stream(:stdio, :line)

    input_stream
    |> stream_lines(all_rules)
    |> Stream.into(output_stream, fn x -> "#{x}\n" end)
    |> Stream.run()
  end
end
