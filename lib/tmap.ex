defmodule TMap do
  require Logger

  @moduledoc """
  Documentation for Tmap.
  """

  @doc """
  apply_rule.
  """
  @spec apply_rule_to_map(map, map) :: [map]
  def apply_rule_to_map(map, cmd) do
    if Map.has_key?(cmd, "action") do
      action = cmd["action"] <> "Plugin"
      Logger.warn("Running action '" <> inspect(action) <> "'")

      {new_acc, _} =
	Code.eval_string(
	  action <> ".run(map, options)",
	  [map: map, options: Map.delete(cmd, cmd["action"])],
	  __ENV__
	)
      new_acc
    else
      Logger.error("Wrong rule: missing 'action' key in '" <> inspect(map) <> "'")
      [map]
    end

  end

    @doc """
  apply_rules.
  """
  @spec apply_rule([map], map) :: [map]
  def apply_rule(maps, cmd) do
    List.foldl(maps, [], fn map, acc ->
      acc ++ apply_rule_to_map(map, cmd)
    end)
  end

  @doc """
  apply_rules.
  """
  @spec apply_rules([map], list, boolean) :: [map]
  def apply_rules(maps, rules, logging \\ false) do
    new_maps = List.foldl(rules, maps, fn cmd, acc ->
	apply_rule(acc, cmd)
    end)

    if logging do
      new_maps
    else
      Logger.debug("Removing key '_log'")
      Enum.map(new_maps, fn(x) -> Map.delete(x, "_log") end)
    end

  end

end
