# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

defmodule Plugin do
  # Define behaviours which user modules have to implement, with type annotations
  @callback transform(map, map) :: [map]

  require Logger

  # When you call use in your module, the __using__ macro is called.
  defmacro __using__(_params) do
    quote do
      # User modules must implement the Filter callbacks
      @behaviour Plugin

      def run(map, options) do
	require Logger
	Logger.debug("run: map size: " <> Integer.to_string(map_size(map)))
	Logger.debug("     map = " <> inspect(map))
	Logger.debug("     options = " <> inspect(options))
	transform(map, options)
	# Logger.debug(map_size(map))
      end
    end
  end
end
