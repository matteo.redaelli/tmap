# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

defmodule KeyCopyPlugin do
  @moduledoc """
  Add a new key/value

  Required parameters:
  - from_key
  - to_key
  - mode
  """
  use Plugin
  require Logger

  @spec transform(map, map) :: [map]
  def transform(dict, options) do
    if not MapUtils.all_required_keys?(options, ["from_key", "to_key", "mode"]) do
      [dict]
    else
      from_key = options["from_key"]
      to_key = options["to_key"]
      value = Map.get(dict, from_key, nil)
      KeysAddPlugin.transform(dict, %{"keys" => %{to_key => value}, "mode" => options["mode"]})
    end
  end

end
