# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

defmodule ValueRegexReplacePlugin do
  @moduledoc """
  Regex.replace for specific keys
  """
  use Plugin
  require Logger

  def transform(dict, options) do
    new_dict = if not MapUtils.all_required_keys?(options, ["keys", "value_regex", "replace_with"]) do
      dict
    else
      Logger.debug("options ='" <> inspect(options) <> "'")
      replace_with = options["replace_with"]

      case RegexUtils.compile(options["value_regex"]) do
	{:error, error} ->
	  Logger.error("Wrong regex '" <> options["value_regex"] <> "' :" <> inspect(error))
	  dict

	{:ok, regex} ->
	  keys_dict = Map.keys(dict)
	  keys_to_change = options["keys"]
	  keys = ListUtils.intersection(keys_dict, keys_to_change)

	  List.foldl(keys, dict, fn key, acc ->
	    new_value = Regex.replace(regex, dict[key], replace_with)

	    acc
	    |> Map.put(key, new_value)
	  end)

	  # fold
      end

      # case
    end
    [new_dict]
  end
end
