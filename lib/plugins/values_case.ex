# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

defmodule ValuesCasePlugin do
  use Plugin
  require Logger

  def transform(dict, options) do
    new_dict = if not MapUtils.all_required_keys?(options, ["keys", "case"]) do
      dict
    else
      keys_dict = Map.keys(dict)
      keys_to_change = options["keys"]
      keys = ListUtils.intersection(keys_dict, keys_to_change)

      List.foldl(keys, dict, fn k, acc ->
	old_value = acc[k]

	new_value =
	  if not is_binary(old_value),
	    do: old_value,
	    else:
	      if(options["case"] == "lower",
		do: String.downcase(old_value),
		else: String.upcase(old_value)
	      )

	if old_value != new_value do
	  Logger.warn(
	    "updating key '" <>
	      k <> "' from value '" <> old_value <> "' to value '" <> new_value <> "'"
	  )

	  Map.replace!(acc, k, new_value)
	else
	  acc
	end
      end)
    end
    [new_dict]
  end
end
