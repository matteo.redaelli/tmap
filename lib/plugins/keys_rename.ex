# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

defmodule KeysRenamePlugin do
  use Plugin
  require Logger

  def transform(dict, options) do
    if not MapUtils.all_required_keys?(options, ["keys", "mode"]) do
      [dict]
    else
      keys = Map.keys(dict)
      keys_candidate = Map.keys(options["keys"])
      keys_to_rename = ListUtils.intersection(keys_candidate, keys)

      List.foldl(keys_to_rename, dict, fn k, acc ->
	new_key = options["keys"][k]
	value = dict[k]

	acc
	|> KeysAddPlugin.transform(%{"keys" => %{new_key => value}, "mode" => options["mode"]})
	|> Enum.at(0)
	|> KeysDeletePlugin.transform(%{"keys" => [k]})
      end)
    end

  end
end
