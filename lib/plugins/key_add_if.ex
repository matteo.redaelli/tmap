# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

defmodule KeyAddIfPlugin do
  @moduledoc """
  Add a new key/value

  Required parameters:
  - key
  - value
  Optional parameters:
  - key_regex
  - value_regex
  """
  use Plugin
  require Logger

  @spec transform(map, map) :: [map]
  def transform(dict, options) do
    cond do
      not MapUtils.all_required_keys?(options, ["key", "value", "mode"]) ->
	[dict]

	not Map.has_key?(options, "key_regex") or
	(MapUtils.match_keys_by_regex?(dict, options["key_regex"]) and
	  not Map.has_key?(options, "value_regex")) or
      MapUtils.match_keys_values_by_regex?(dict, options["key_regex"], options["value_regex"]) ->
	Logger.warn("adding key '" <> options["key"] <> "'")
	new_key = options["key"]
	value = options["value"]
	KeysAddPlugin.transform(dict, %{"keys" => %{new_key => value}, "mode" => options["mode"]})

      true ->
	[dict]
    end
  end

end
