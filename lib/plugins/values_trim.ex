# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

defmodule ValuesTrimPlugin do
  @moduledoc """
  String.trim for all values of the map
  """
  use Plugin
  require Logger

  @spec transform(map, map) :: [map]
  def transform(dict, _options \\ %{}) do
    new_dict = List.foldl(Map.keys(dict), dict, fn key, acc ->
      old_value = acc[key]
      new_value = if is_binary(old_value), do: String.trim(old_value), else: old_value

      if old_value != new_value do
	Logger.warn(
	  "updating key '" <>
	    key <> "' from value '" <> old_value <> "' to value '" <> new_value <> "'"
	)

	acc
	|> Map.replace!(key, new_value)
      else
	acc
      end

      # if
    end)
    [new_dict]
  end
end
