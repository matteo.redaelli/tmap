# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

defmodule ValueRegexNamedCapturesPlugin do
  use Plugin
  require Logger

  def transform(dict, options) do
    new_dict = if not MapUtils.all_required_keys?(options, ["key", "value_regex"]) do
      dict
    else
      Logger.debug("options ='" <> inspect(options) <> "'")

      case RegexUtils.compile(options["value_regex"]) do
	{:error, error} ->
	  Logger.error("Wrong regex '" <> options["value_regex"] <> "' :" <> inspect(error))
	  dict

	{:ok, regex} ->
	  key = options["key"]

	  case Map.fetch(dict, key) do
	    :error ->
	      Logger.error("Missing key '" <> options["key"])
	      dict

	    {:ok, value} ->
	      new_map = Regex.named_captures(regex, value)

	      if new_map do
		Map.merge(dict, new_map)
	      else
		dict
	      end
	  end
      end

      # case
    end
    [new_dict]
  end
end
