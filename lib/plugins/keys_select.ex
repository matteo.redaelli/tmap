# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

defmodule KeysSelectPlugin do
  use Plugin
  require Logger

  def transform(dict, options) do
    if not MapUtils.all_required_keys?(options, ["keys"]) do
      [%{}]
    else
      old_keys = Map.keys(dict)
      keys = options["keys"]

      new_dict = List.foldl(keys, %{}, fn k, acc ->
	value = if Enum.member?(old_keys, k), do: dict[k], else: Nil

	acc
	|> KeysAddPlugin.transform(%{"keys" => %{k => value}, "mode" => "overwrite"})
	|> Enum.at(0)
      end)
      [new_dict]
    end
  end
end
