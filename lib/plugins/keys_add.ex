# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

defmodule KeysAddPlugin do
  use Plugin
  require Logger

  @spec transform(map, map) :: [map]
  def transform(dict, options) do
    new_dict = if not MapUtils.all_required_keys?(options, ["keys", "mode"]) do
      dict
    else
      keys = Map.keys(options["keys"])
      mode = options["mode"]

      List.foldl(keys, dict, fn k, acc ->
	v = options["keys"][k]

	case mode do
	  "overwrite" ->
	    acc
	    |> Map.put(k, v)
	    |> MapUtils.log("Added key (overwrite) " <> k)

	  "no_overwrite" ->
	    acc
	    |> Map.put_new(k, v)
	    |> MapUtils.log("Added key (no_overwrite) " <> k <> " with value " <> inspect(v))

	  _ ->
	    Logger.error("unknown mode '" <> mode <> "': skipping adding key/value")
	    acc
	end
      end)
    end
    [new_dict]
  end

end
